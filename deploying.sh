#!/usr/bin/env bash
set -eux

git fetch --all
# git reset --hard origin/fbbot
# docker build -f Dockerfile -t bot_core .

git reset --hard origin/master
docker build -f Dockerfile_dashboard -t bot_dashboard .


# docker build -f Dockerfile_for_updated -t pro_car2 .
# docker build -f Dockerfile_journeys -t pro_car_journeys .
# docker build -f Dockerfile -t pro_car_post_processing .

# docker swarm init
docker stack rm dellbot

sleep 20s # Waits 20 seconds.

docker stack deploy -c "docker-compose-bot.yml" dellbot --prune

# docker run --rm --name pro_car1 -d pro_car1:latest worker.js
# docker-compose -f "docker-compose.yml" up -d --remove-orphans
