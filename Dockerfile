FROM node:9

RUN mkdir -p /workspace
WORKDIR /workspace

ADD package.json .
ADD . .

RUN npm install

EXPOSE 3000

RUN npm run build

# ENV BACK_TO=0
# ENV SKIP=0
# ENV LIMIT=0
# ENV JUST_UPDATED_ONLY=0

CMD [ "npm", "start" ]