#!/usr/bin/env bash
set -eux

MACHINE_1=13.251.49.76

ssh ubuntu@$MACHINE_1 << EOF
   #some commands
   cd ~/projects/dellbot
   sh deploying.sh
EOF