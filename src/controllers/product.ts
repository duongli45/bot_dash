import { Request, Response, NextFunction } from "express";
import { default as Template, TemplateModel } from "../models/Template";
import { WriteError } from "mongodb";

const TYPE_PRODUCT = "2";
const TYPE_CATEGORY = "1";
/**
 * GET /
 * Home page.
 */
export let index = (req: Request, res: Response) => {
  Template.find({ type: TYPE_PRODUCT }, (err, results) => {
    res.render("product/index", {
      title: "Product Management",
      list_product : results
    });
  });
};

export let getAddProduct = (req: Request, res: Response, next: NextFunction) => {
    Template.find({ type: TYPE_CATEGORY }, (err, results) => {
      res.render("product/add", {
        title: "Add New Product",
        list_category : results
      });
    });
};

export let postAddProduct = (req: Request, res: Response, next: NextFunction) => {
  req.assert("template_name", "Product name cannot be blank").notEmpty();
  req.assert("image_url", "Image' URL cannot be blank").notEmpty();
  req.assert("detail_url", "Product detail URL cannot be blank").notEmpty();
  const errors = req.validationErrors();

  if (errors) {
    req.flash("errors", errors);
    return res.redirect("/product/add");
  }

  Template.findOne({ type: TYPE_PRODUCT, template_name: req.body.template_name }, (err, existingTemplate: TemplateModel) => {
    if (err) { return next(err); }
    if (existingTemplate) {
      req.flash("errors", { msg: "The product you're trying to add is already exists." });
      return res.redirect("/product/add");
    } else {
      const timestamp = new Date().getTime() + Math.random();
      const product = new Template({
        template_id: "product_" + timestamp,
        template_name: req.body.template_name,
        additional_text: req.body.additional_text,
        image_url: req.body.image_url,
        detail_url: req.body.detail_url,
        parent_id: req.body.parent_id,
        type: TYPE_PRODUCT
      });
      product.save((err: WriteError) => {
        if (err) {
          req.flash("errors", err);
          return next(err);
        }
      });
      console.debug(product);
    }
  });

  req.flash("success", { msg: "Your setting has been saved." });
  res.redirect("/product");
};

export let getDetailsProduct = (req: Request, res: Response, next: NextFunction) => {
  Template.findOne({ type: TYPE_PRODUCT, template_id : req.params.id }, (err, result: TemplateModel) => {
    if (result) {
      Template.findOne({ type: TYPE_CATEGORY, template_id : result.parent_id }, (err, resultCategory) => {
        if (resultCategory) {
          res.render("product/details", {
            title: "Product Details",
            product : result,
            category : resultCategory
          });
        } else {
          res.redirect("/product");
        }
      });
    } else {
      res.redirect("/product");
    }
  });
};

export let getEditProduct = (req: Request, res: Response, next: NextFunction) => {
  Template.findOne({ type: TYPE_PRODUCT, template_id : req.params.id }, (err, result: TemplateModel) => {
    if (result) {
      Template.find({ type: TYPE_CATEGORY }, (err, results) => {
        res.render("product/edit", {
          title: "Product Update",
          product : result,
          list_category : results
        });
      });
    } else {
      res.redirect("/product");
    }
  });
};

export let postEditProduct = (req: Request, res: Response, next: NextFunction) => {
  req.assert("template_name", "Product name cannot be blank").notEmpty();
  req.assert("image_url", "Image' URL cannot be blank").notEmpty();
  const errors = req.validationErrors();

  if (errors) {
    req.flash("errors", errors);
    return res.redirect("/product/add");
  }

  Template.findOne({ type: TYPE_PRODUCT, template_id: req.body.template_id }, (err, existingTemplate: TemplateModel) => {
    if (err) { return next(err); }
    if (existingTemplate) {
      existingTemplate.template_name = req.body.template_name || "";
      existingTemplate.additional_text = req.body.additional_text || "";
      existingTemplate.image_url = req.body.image_url || "";
      existingTemplate.detail_url = req.body.detail_url || "";
      existingTemplate.parent_id = req.body.parent_id || "";
      existingTemplate.save((err: WriteError) => {
        if (err) {
          req.flash("errors", err);
          return next(err);
        }
      });
    } else {
      req.flash("errors", { msg: "The product you're trying to update is not exists." });
      return res.redirect("/product");
    }
  });
  req.flash("success", { msg: "Your setting has been saved." });
  res.redirect("/product/details/" + req.body.template_id);
};

export let postDeleteProduct = (req: Request, res: Response, next: NextFunction) => {
  Template.findOneAndRemove({ type: TYPE_PRODUCT, template_id: req.params.id }, (err, existingTemplate: TemplateModel) => {
    let results = "";
    if (err) {
      req.flash("errors", { msg: "The product you're trying to delete is not exists." });
      results = err.message;
    } else {
      req.flash("success", { msg: "Delete success!" });
      results = "Deleted!";
    }
    res.json({ result: results });
  });
};