import { Request, Response, NextFunction } from "express";
import { default as Template, TemplateModel } from "../models/Template";
import { WriteError } from "mongodb";

const TYPE_CATEGORY = "1";

/**
 * GET /
 * Home page.
 */
export let index = (req: Request, res: Response) => {
  Template.find({ type: TYPE_CATEGORY }, (err, results) => {
    res.render("category/index", {
      title: "Category Management",
      list_category : results
    });
  });
};

export let getAddCategory = (req: Request, res: Response, next: NextFunction) => {
    res.render("category/add", {
      title: "Add New Category"
    });
};

export let postAddCategory = (req: Request, res: Response, next: NextFunction) => {
  req.assert("template_name", "Category name cannot be blank").notEmpty();
  req.assert("image_url", "Image' URL cannot be blank").notEmpty();
  const errors = req.validationErrors();

  if (errors) {
    req.flash("errors", errors);
    return res.redirect("/category/add");
  }

  Template.findOne({ type: "1", template_name: req.body.template_name }, (err, existingTemplate: TemplateModel) => {
    if (err) { return next(err); }
    if (existingTemplate) {
      req.flash("errors", { msg: "The category you're trying to add is already exists." });
      return res.redirect("/category/add");
    } else {
      const timestamp = new Date().getTime() + Math.random();
      const category = new Template({
        template_id: "category_" + timestamp,
        template_name: req.body.template_name,
        additional_text: req.body.additional_text,
        image_url: req.body.image_url,
        type: "1"
      });
      category.save((err: WriteError) => {
        if (err) {
          req.flash("errors", err);
          return next(err);
        }
      });
      console.debug(category);
    }
  });

  req.flash("success", { msg: "Your setting has been saved." });
  res.redirect("/category");
};

export let getDetailsCategory = (req: Request, res: Response, next: NextFunction) => {
  Template.findOne({ type: "1", template_id : req.params.id }, (err, result: TemplateModel) => {
    if (result) {
      res.render("category/details", {
        title: "Category Details",
        category : result
      });
    } else {
      res.redirect("/category");
    }
  });
};

export let getEditCategory = (req: Request, res: Response, next: NextFunction) => {
  Template.findOne({ type: "1", template_id : req.params.id }, (err, result: TemplateModel) => {
    if (result) {
      res.render("category/edit", {
        title: "Category Update",
        category : result
      });
    } else {
      res.redirect("/category");
    }
  });
};

export let postEditCategory = (req: Request, res: Response, next: NextFunction) => {
  req.assert("template_name", "Category name cannot be blank").notEmpty();
  req.assert("image_url", "Image' URL cannot be blank").notEmpty();
  const errors = req.validationErrors();

  if (errors) {
    req.flash("errors", errors);
    return res.redirect("/category/add");
  }

  Template.findOne({ type: "1", template_id: req.body.template_id }, (err, existingTemplate: TemplateModel) => {
    if (err) { return next(err); }
    if (existingTemplate) {
      existingTemplate.template_name = req.body.template_name || "";
      existingTemplate.additional_text = req.body.additional_text || "";
      existingTemplate.image_url = req.body.image_url || "";
      existingTemplate.save((err: WriteError) => {
        if (err) {
          req.flash("errors", err);
          return next(err);
        }
      });
    } else {
      req.flash("errors", { msg: "The category you're trying to update is not exists." });
      return res.redirect("/category");
    }
  });
  req.flash("success", { msg: "Your setting has been saved." });
  res.redirect("/category/details/" + req.body.template_id);
};

export let postDeleteCategory = (req: Request, res: Response, next: NextFunction) => {
  Template.findOneAndRemove({ type: "1", template_id: req.params.id }, (err, existingTemplate: TemplateModel) => {
    let results = "";
    if (err) {
      req.flash("errors", { msg: "The category you're trying to delete is not exists." });
      results = err.message;
    } else {
      req.flash("success", { msg: "Delete success!" });
      results = "Deleted!";
    }
    res.json({ result: results });
  });
};