import { Request, Response, NextFunction } from "express";
import { default as Template, TemplateModel } from "../models/Template";
import { WriteError } from "mongodb";
import mongoose from "mongoose";
import moment from "moment";

const TYPE_PROMOTION = "3";
/**
 * GET /
 * Home page.
 */
export let index = (req: Request, res: Response) => {
    Template.find({ type: TYPE_PROMOTION }, (err, results: Array<TemplateModel>) => {
        const returnRes: any = [];
        results.forEach((template) => {
            const temp: any = JSON.parse(JSON.stringify(template));
            temp.start_date = moment(temp.start_date).format("MM/DD/YYYY hh:mm A");
            temp.end_date = moment(temp.end_date).format("MM/DD/YYYY hh:mm A");
            returnRes.push(temp);
        });
        res.render("promotion/index", {
            title: "Promotion Management",
            list_promotion: returnRes
        });
    });
};

export let getAddPromotion = (req: Request, res: Response, next: NextFunction) => {
    res.render("promotion/add", {
        title: "Add New Promotion"
    });
};

export let postAddPromotion = (req: Request, res: Response, next: NextFunction) => {
    req.assert("template_name", "Promotion name cannot be blank").notEmpty();
    req.assert("image_url", "Image' URL cannot be blank").notEmpty();
    req.assert("detail_url", "Promotion detail URL cannot be blank").notEmpty();
    const errors = req.validationErrors();

    if (errors) {
        req.flash("errors", errors);
        return res.redirect("/promotion/add");
    }

    Template.findOne({ type: TYPE_PROMOTION, template_name: req.body.template_name }, (err, existingTemplate: TemplateModel) => {
        if (err) { return next(err); }
        if (existingTemplate) {
            req.flash("errors", { msg: "The promotion you're trying to add is already exists." });
            return res.redirect("/promotion/add");
        } else {
            const timestamp = new Date().getTime() + Math.random();
            const promotion = new Template({
                template_id: "promotion_" + timestamp,
                template_name: req.body.template_name,
                additional_text: req.body.additional_text,
                image_url: req.body.image_url,
                detail_url: req.body.detail_url,
                start_date: new Date(req.body.start_date),
                end_date: new Date(req.body.end_date),
                message: req.body.message,
                type: TYPE_PROMOTION
            });
            promotion.save((err: WriteError) => {
                if (err) {
                    req.flash("errors", err);
                    return next(err);
                }
            });
        }
    });

    req.flash("success", { msg: "Your setting has been saved." });
    res.redirect("/promotion");
};

export let getDetailPromotion = (req: Request, res: Response, next: NextFunction) => {
    Template.findOne({ type: TYPE_PROMOTION, _id: mongoose.Types.ObjectId(req.params.id) }, (err, result: TemplateModel) => {
        if (result) {
            res.render("promotion/detail", {
                title: "Promotion Detail",
                promotion: result
            });
        } else {
            res.redirect("/promotion");
        }
    });
};

export let getEditPromotion = (req: Request, res: Response, next: NextFunction) => {
    Template.findOne({ type: TYPE_PROMOTION, _id: mongoose.Types.ObjectId(req.params.id) }, (err, result: TemplateModel) => {
        if (result) {
            const temp = JSON.parse(JSON.stringify(result));
            temp.start_date = moment(temp.start_date).format("MM/DD/YYYY hh:mm A");
            temp.end_date = moment(temp.end_date).format("MM/DD/YYYY hh:mm A");

            res.render("promotion/edit", {
                title: "Promotion Update",
                promotion: temp
            });
        } else {
            res.redirect("/promotion");
        }
    });
};

export let postEditPromotion = (req: Request, res: Response, next: NextFunction) => {
    req.assert("template_name", "Promotion name cannot be blank").notEmpty();
    req.assert("image_url", "Image's URL cannot be blank").notEmpty();
    req.assert("start_date", "Start date cannot be blank").notEmpty();
    req.assert("end_date", "End date cannot be blank").notEmpty();
    req.assert("message", "Message URL cannot be blank").notEmpty();
    const errors = req.validationErrors();

    if (errors) {
        req.flash("errors", errors);
        return res.redirect("/promotion/add");
    }

    Template.findOne({ type: TYPE_PROMOTION, _id: mongoose.Types.ObjectId(req.params.id) }, (err, existingTemplate: TemplateModel) => {
        if (err) { return next(err); }
        if (existingTemplate) {
            existingTemplate.template_name = req.body.template_name || "";
            existingTemplate.additional_text = req.body.additional_text || "";
            existingTemplate.image_url = req.body.image_url || "";
            existingTemplate.detail_url = req.body.detail_url || "";
            existingTemplate.parent_id = req.body.parent_id || "";
            existingTemplate.start_date = new Date(req.body.start_date) || new Date();
            existingTemplate.end_date = new Date(req.body.end_date) || new Date();
            existingTemplate.message = req.body.message || "";
            existingTemplate.save((err: WriteError) => {
                if (err) {
                    req.flash("errors", err);
                    return next(err);
                }
            });
        } else {
            req.flash("errors", { msg: "The promotion you're trying to update is not exists." });
            return res.redirect("/promotion");
        }
    });
    req.flash("success", { msg: "Your setting has been saved." });
    res.redirect("/promotion/detail/" + req.params.id);
};

export let postDeletePromotion = (req: Request, res: Response, next: NextFunction) => {
    Template.findOneAndRemove({ type: TYPE_PROMOTION, _id: mongoose.Types.ObjectId(req.params.id) }, (err, existingTemplate: TemplateModel) => {
        let results = "";
        if (err) {
            req.flash("errors", { msg: "The promotion you're trying to delete doesn't exist." });
            results = err.message;
        } else {
            req.flash("success", { msg: "Deleting successfully" });
            results = "Deleted";
        }
        res.json({ result: results });
    });
};