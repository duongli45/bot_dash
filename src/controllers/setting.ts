import { Request, Response, NextFunction } from "express";
import { default as Template, TemplateModel } from "../models/Template";
import { default as Message, MessageModel } from "../models/Message";
import { WriteError } from "mongodb";

/**
 * Setting constraint
 */
const LANG_VIE_CODE = "vi_VN";
const LANG_ENG_CODE = "en_US";

const MESSAGE_ID_WELCOME = "welcome";
const MESSAGE_ID_GREETING = "greeting";
const TEMPLATE_ID_MINIGAME = "minigame_url";
const TEMPLATE_ID_FAQS = "faqs_url";

const TYPE_MINIGAME = "4";
const TYPE_FAQS = "5";

export let getSettingURL = (req: Request, res: Response, next: NextFunction) => {
  let minigame_url = "";
  let faqs_url = "";
  Template.findOne({ template_id: TEMPLATE_ID_MINIGAME }, (err, existingTemplate: TemplateModel) => {
    if (err) { return next(err); }
    if (existingTemplate) {
      minigame_url = existingTemplate.detail_url;
    }
    Template.findOne({ template_id: TEMPLATE_ID_FAQS }, (err, existingTemplate: TemplateModel) => {
      if (err) { return next(err); }
      if (existingTemplate) {
        faqs_url = existingTemplate.detail_url;
      }
      res.render("setting/url-setting", {
        title: "URL Setting",
        minigame_url: minigame_url,
        faqs_url: faqs_url
      });
    });
  });
};

export let postSettingURL = (req: Request, res: Response, next: NextFunction) => {
  req.assert("minigame_url", "Minigame's URL cannot be blank").notEmpty();
  req.assert("faqs_url", "FAQs' URL cannot be blank").notEmpty();
  req.assert("minigame_url", "Minigame's URL must be an url").isURL();
  req.assert("faqs_url", "FAQs' URL must be an url").isURL();
  const errors = req.validationErrors();

  if (errors) {
    req.flash("errors", errors);
    return res.redirect("/setting/url-setting");
  }

  // check if message exist or not
  Template.findOne({ template_id: TEMPLATE_ID_MINIGAME }, (err, existingTemplate: TemplateModel) => {
    if (err) { return next(err); }
    if (existingTemplate) {
      existingTemplate.detail_url = req.body.minigame_url || "";
      existingTemplate.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    } else {
      const message = new Template({
        template_id: TEMPLATE_ID_MINIGAME,
        detail_url: req.body.minigame_url,
        type: TYPE_MINIGAME
      });
      message.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    }
  });
  Template.findOne({ template_id: TEMPLATE_ID_FAQS}, (err, existingTemplate: TemplateModel) => {
    if (err) { return next(err); }
    if (existingTemplate) {
      existingTemplate.detail_url = req.body.faqs_url || "";
      existingTemplate.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    } else {
      const message = new Template({
        template_id: TEMPLATE_ID_FAQS,
        detail_url: req.body.faqs_url,
        type: TYPE_FAQS
      });
      message.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    }
  });

  req.flash("success", { msg: "Your setting has been saved." });
  res.redirect("/setting/url-setting");
};

export let getWelcomeMessage = (req: Request, res: Response, next: NextFunction) => {
  let welcome_vie = "";
  let welcome_eng = "";
  Message.findOne({ message_id: MESSAGE_ID_WELCOME, message_language : LANG_VIE_CODE }, (err, existingMessage: MessageModel) => {
    if (err) { return next(err); }
    if (existingMessage) {
      welcome_vie = existingMessage.message_content;
    }
    Message.findOne({ message_id: MESSAGE_ID_WELCOME, message_language : LANG_ENG_CODE }, (err, existingMessage: MessageModel) => {
      if (err) { return next(err); }
      if (existingMessage) {
        welcome_eng = existingMessage.message_content;
      }
      res.render("setting/welcome-message", {
        title: "Welcome Message",
        welcome_vie: welcome_vie,
        welcome_eng: welcome_eng
      });
    });
  });
};

export let postWelcomeMessage = (req: Request, res: Response, next: NextFunction) => {
  req.assert("welcome_eng", "Message(English) cannot be blank").notEmpty();
  req.assert("welcome_vie", "Message(Vietnamese) cannot be blank").notEmpty();
  const errors = req.validationErrors();

  if (errors) {
    req.flash("errors", errors);
    return res.redirect("/setting/welcome-message");
  }

  // check if message exist or not
  Message.findOne({ message_id: MESSAGE_ID_WELCOME, message_language : LANG_VIE_CODE }, (err, existingMessage: MessageModel) => {
    if (err) { return next(err); }
    if (existingMessage) {
      existingMessage.message_content = req.body.welcome_vie || "";
      existingMessage.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    } else {
      const message = new Message({
        message_id: MESSAGE_ID_WELCOME,
        message_language: LANG_VIE_CODE,
        message_content: req.body.welcome_vie
      });
      message.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    }
  });
  Message.findOne({ message_id: MESSAGE_ID_WELCOME, message_language : LANG_ENG_CODE }, (err, existingMessage: MessageModel) => {
    if (err) { return next(err); }
    if (existingMessage) {
      existingMessage.message_content = req.body.welcome_eng || "";
      existingMessage.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    } else {
      const message = new Message({
        message_id: MESSAGE_ID_WELCOME,
        message_language: LANG_ENG_CODE,
        message_content: req.body.welcome_eng
      });
      message.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    }
  });

  req.flash("success", { msg: "Message has been saved." });
  res.redirect("/setting/welcome-message");
};

export let getGreetingMessage = (req: Request, res: Response, next: NextFunction) => {
  let greeting_vie = "";
  let greeting_eng = "";
  Message.findOne({ message_id: MESSAGE_ID_GREETING, message_language : LANG_VIE_CODE }, (err, existingMessage: MessageModel) => {
    if (err) { return next(err); }
    if (existingMessage) {
      greeting_vie = existingMessage.message_content;
    }
    Message.findOne({ message_id: MESSAGE_ID_GREETING, message_language : LANG_ENG_CODE }, (err, existingMessage: MessageModel) => {
      if (err) { return next(err); }
      if (existingMessage) {
        greeting_eng = existingMessage.message_content;
      }
      res.render("setting/greeting-message", {
        title: "Greeting Message",
        greeting_vie: greeting_vie,
        greeting_eng: greeting_eng
      });
    });
  });
};

export let postGreetingMessage = (req: Request, res: Response, next: NextFunction) => {
  req.assert("greeting_eng", "Message(English) cannot be blank").notEmpty();
  req.assert("greeting_vie", "Message(Vietnamese) cannot be blank").notEmpty();
  const errors = req.validationErrors();

  if (errors) {
    req.flash("errors", errors);
    return res.redirect("/setting/greeting-message");
  }

  // check if message exist or not
  Message.findOne({ message_id: MESSAGE_ID_GREETING, message_language : LANG_VIE_CODE }, (err, existingMessage: MessageModel) => {
    if (err) { return next(err); }
    if (existingMessage) {
      existingMessage.message_content = req.body.greeting_vie || "";
      existingMessage.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    } else {
      const message = new Message({
        message_id: MESSAGE_ID_GREETING,
        message_language: LANG_VIE_CODE,
        message_content: req.body.greeting_vie
      });
      message.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    }
  });
  Message.findOne({ message_id: MESSAGE_ID_GREETING, message_language : LANG_ENG_CODE }, (err, existingMessage: MessageModel) => {
    if (err) { return next(err); }
    if (existingMessage) {
      existingMessage.message_content = req.body.greeting_eng || "";
      existingMessage.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    } else {
      const message = new Message({
        message_id: MESSAGE_ID_GREETING,
        message_language: LANG_ENG_CODE,
        message_content: req.body.greeting_eng
      });
      message.save((err: WriteError) => {
        if (err) {
          return next(err);
        }
      });
    }
  });

  req.flash("success", { msg: "Message has been saved." });
  res.redirect("/setting/greeting-message");
};
