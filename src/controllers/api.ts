"use strict";

import async from "async";
import request from "request";
import graph from "fbgraph";
import { Response, Request, NextFunction } from "express";
import * as autoAnswerController from "./autoAnswer";
import * as qs from "querystring";
import HttpStatus from "http-status-codes";


/**
 * GET /api
 * List of API examples.
 */
export let getApi = (req: Request, res: Response) => {
  res.render("api/index", {
    title: "API Examples"
  });
};

/**
 * GET /api/facebook
 * Facebook API example.
 */
export let getFacebook = (req: Request, res: Response, next: NextFunction) => {
  const token = req.user.tokens.find((token: any) => token.kind === "facebook");
  graph.setAccessToken(token.accessToken);
  graph.get(`${req.user.facebook}?fields=id,name,email,first_name,last_name,gender,link,locale,timezone`, (err: Error, results: graph.FacebookUser) => {
    if (err) { return next(err); }
    res.render("api/facebook", {
      title: "Facebook API",
      profile: results
    });
  });
};

export let getAutoAnswer = async (req: Request, res: Response, next: NextFunction) => {
  const result = await autoAnswerController.getAnswer(req.query);

  if (!result) {
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).send("Internal server error");
  } else {
    if (result === "NF") {
      res.status(HttpStatus.NOT_FOUND).send("Answer not found");
    }

    res.status(HttpStatus.OK).send(result);
  }
};
