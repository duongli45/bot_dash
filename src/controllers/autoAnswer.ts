import { Request, Response, NextFunction } from "express";
import { default as AutoAnswer, AutoAnswerModel } from "../models/AutoAnswer";
import { WriteError } from "mongodb";
import mongoose from "mongoose";

/**
 * GET /
 * Home page.
 */
export let index = (req: Request, res: Response) => {
  AutoAnswer.find({}, (err, results) => {
    res.render("autoanswer/index", {
      title: "Auto Answer",
      list_answer : results
    });
  });
};

export let getAddAnswer = (req: Request, res: Response) => {
    res.render("autoanswer/add", {
      title: "Add New Answer"
  });
};

export let postAddAnswer = (req: Request, res: Response, next: NextFunction) => {
  req.assert("keyword", "Answer keyword cannot be blank").notEmpty();
  req.assert("message_language", "Answer language cannot be blank").notEmpty();
  req.assert("message_content", "Answer content cannot be blank").notEmpty();
  const errors = req.validationErrors();

  if (errors) {
    req.flash("errors", errors);
    return res.redirect("/answer/add");
  }

  AutoAnswer.findOne({
      keyword: req.body.keyword,
      message_language: req.body.message_language
    }, (err, existingAnswer: AutoAnswerModel) => {
    if (err) { return next(err); }
    if (existingAnswer) {
      req.flash("errors", { msg: "The answer you're trying to add already exists." });
      return res.redirect("/answer/add");
    } else {
      const answer = new AutoAnswer({
        keyword: req.body.keyword,
        message_language: req.body.message_language,
        message_content: req.body.message_content
      });
      answer.save((err: WriteError) => {
        if (err) {
          req.flash("errors", err);
          return next(err);
        }
      });
    }
  });

  req.flash("success", { msg: "Your action has been saved." });
  res.redirect("/autoanswer");
};

export let getEditAnswer = (req: Request, res: Response, next: NextFunction) => {
  AutoAnswer.findOne({ _id: mongoose.Types.ObjectId(req.params.id) }, (err, result: AutoAnswerModel) => {
    if (result) {
      res.render("autoanswer/edit", {
        title: "Answer Edit",
        answer: result
      });
    } else {
      res.redirect("/autoanswer");
    }
  });
};

export let postEditAnswer = (req: Request, res: Response, next: NextFunction) => {
  req.assert("keyword", "Answer keyword cannot be blank").notEmpty();
  req.assert("message_language", "Answer language cannot be blank").notEmpty();
  req.assert("message_content", "Answer content cannot be blank").notEmpty();
  const errors = req.validationErrors();

  if (errors) {
    req.flash("errors", errors);
    return res.redirect("/autoanswer/add");
  }

  AutoAnswer.findOne({ _id: mongoose.Types.ObjectId(req.params.id) }, (err, existingAnswer: AutoAnswerModel) => {
    if (err) { return next(err); }
    if (existingAnswer) {
      existingAnswer.keyword = req.body.keyword || "";
      existingAnswer.message_language = req.body.message_language || "vi";
      existingAnswer.message_content = req.body.message_content || "";
      existingAnswer.save((err: WriteError) => {
        if (err) {
          req.flash("errors", err);
          return next(err);
        }
      });
    } else {
      req.flash("errors", { msg: "The answer you're trying to update doesn't exist." });
      return res.redirect("/answer");
    }
  });
  req.flash("success", { msg: "Your setting has been saved." });
  res.redirect("/autoanswer");
};

export let postDeleteAnswer = (req: Request, res: Response, next: NextFunction) => {
  AutoAnswer.findOneAndRemove({ _id: mongoose.Types.ObjectId(req.params.id) }, (err, existingAnswer: AutoAnswerModel) => {
    let results = "";
    if (err) {
      req.flash("errors", { msg: "The answer you're trying to delete doesn't exist." });
      results = err.message;
    } else {
      req.flash("success", { msg: "Deleting successfully!" });
      results = "Deleted";
    }
    res.json({ result: results });
  });
};

// For api
export let getAnswer = async  (query: any) => {
  return new Promise((resolve, reject) => {
    AutoAnswer.findOne({ keyword: query.keyword, message_language: query.language }, (err, result: AutoAnswerModel) => {
      if (err) { reject(undefined); }

      if (result) {
        resolve(result);
      } else {
        resolve("NF");
      }
    });
  });
};
