import express from "express";
import compression from "compression";  // compresses requests
import session from "express-session";
import bodyParser from "body-parser";
import logger from "./util/logger";
import lusca from "lusca";
import dotenv from "dotenv";
import mongo from "connect-mongo";
import flash from "express-flash";
import path from "path";
import mongoose from "mongoose";
import passport from "passport";
import expressValidator from "express-validator";
import bluebird from "bluebird";
import { MONGODB_URI, SESSION_SECRET } from "./util/secrets";

const MongoStore = mongo(session);

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: ".env.example" });

// Controllers (route handlers)
import * as homeController from "./controllers/home";
import * as userController from "./controllers/user";
import * as apiController from "./controllers/api";
import * as contactController from "./controllers/contact";
import * as settingController from "./controllers/setting";
import * as categoryController from "./controllers/category";
import * as productController from "./controllers/product";
import * as autoAnswerController from "./controllers/autoAnswer";
import * as promotionController from "./controllers/promotion";


// API keys and Passport configuration
import * as passportConfig from "./config/passport";

// Create Express server
const app = express();

// Connect to MongoDB
const mongoUrl = MONGODB_URI;
(<any>mongoose).Promise = bluebird;
mongoose.connect(mongoUrl, {useMongoClient: true}).then(
  () => { /** ready to use. The `mongoose.connect()` promise resolves to undefined. */ },
).catch(err => {
  console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
  // process.exit();
});

// Express configuration
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "../views"));
app.set("view engine", "pug");
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: SESSION_SECRET,
  store: new MongoStore({
    url: mongoUrl,
    autoReconnect: true
  })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(lusca.xframe("SAMEORIGIN"));
app.use(lusca.xssProtection(true));
app.use((req, res, next) => {
  res.locals.user = req.user;
  next();
});
app.use((req, res, next) => {
  // After successful login, redirect back to the intended page
  if (!req.user &&
    req.path !== "/login" &&
    req.path !== "/signup" &&
    !req.path.match(/^\/auth/) &&
    !req.path.match(/\./)) {
    req.session.returnTo = req.path;
  } else if (req.user &&
    req.path == "/account") {
    req.session.returnTo = req.path;
  }
  next();
});

app.use(
  express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })
);

/**
 * Primary app routes.
 */
app.get("/", passportConfig.isAuthenticated, homeController.index);
app.get("/login", userController.getLogin);
app.post("/login", userController.postLogin);
app.get("/logout", userController.logout);
app.get("/forgot", userController.getForgot);
app.post("/forgot", userController.postForgot);
app.get("/reset/:token", userController.getReset);
app.post("/reset/:token", userController.postReset);
app.get("/signup", userController.getSignup);
app.post("/signup", userController.postSignup);
app.get("/contact", contactController.getContact);
app.post("/contact", contactController.postContact);
app.get("/account", passportConfig.isAuthenticated, userController.getAccount);
app.post("/account/profile", passportConfig.isAuthenticated, userController.postUpdateProfile);
app.post("/account/password", passportConfig.isAuthenticated, userController.postUpdatePassword);
app.post("/account/delete", passportConfig.isAuthenticated, userController.postDeleteAccount);
app.get("/account/unlink/:provider", passportConfig.isAuthenticated, userController.getOauthUnlink);
app.get("/setting/welcome-message", passportConfig.isAuthenticated, settingController.getWelcomeMessage);
app.post("/setting/welcome-message", passportConfig.isAuthenticated, settingController.postWelcomeMessage);
app.get("/setting/greeting-message", passportConfig.isAuthenticated, settingController.getGreetingMessage);
app.post("/setting/greeting-message", passportConfig.isAuthenticated, settingController.postGreetingMessage);
app.get("/setting/url-setting", passportConfig.isAuthenticated, settingController.getSettingURL);
app.post("/setting/url-setting", passportConfig.isAuthenticated, settingController.postSettingURL);
app.get("/category", passportConfig.isAuthenticated, categoryController.index);
app.get("/category/add", passportConfig.isAuthenticated, categoryController.getAddCategory);
app.post("/category/add", passportConfig.isAuthenticated, categoryController.postAddCategory);
app.get("/category/details/:id", passportConfig.isAuthenticated, categoryController.getDetailsCategory);
app.get("/category/edit/:id", passportConfig.isAuthenticated, categoryController.getEditCategory);
app.post("/category/edit/:id", passportConfig.isAuthenticated, categoryController.postEditCategory);
app.post("/category/delete/:id", passportConfig.isAuthenticated, categoryController.postDeleteCategory);
app.get("/product", passportConfig.isAuthenticated, productController.index);
app.get("/product/add", passportConfig.isAuthenticated, productController.getAddProduct);
app.post("/product/add", passportConfig.isAuthenticated, productController.postAddProduct);
app.get("/product/details/:id", passportConfig.isAuthenticated, productController.getDetailsProduct);
app.get("/product/edit/:id", passportConfig.isAuthenticated, productController.getEditProduct);
app.post("/product/edit/:id", passportConfig.isAuthenticated, productController.postEditProduct);
app.post("/product/delete/:id", passportConfig.isAuthenticated, productController.postDeleteProduct);
app.get("/autoanswer", passportConfig.isAuthenticated, autoAnswerController.index);
app.get("/autoanswer/add", passportConfig.isAuthenticated, autoAnswerController.getAddAnswer);
app.post("/autoanswer/add", passportConfig.isAuthenticated, autoAnswerController.postAddAnswer);
app.get("/autoanswer/edit/:id", passportConfig.isAuthenticated, autoAnswerController.getEditAnswer);
app.post("/autoanswer/edit/:id", passportConfig.isAuthenticated, autoAnswerController.postEditAnswer);
app.post("/autoanswer/delete/:id", passportConfig.isAuthenticated, autoAnswerController.postDeleteAnswer);
app.get("/promotion/", passportConfig.isAuthenticated, promotionController.index);
app.get("/promotion/add", passportConfig.isAuthenticated, promotionController.getAddPromotion);
app.post("/promotion/add", passportConfig.isAuthenticated, promotionController.postAddPromotion);
app.get("/promotion/edit/:id", passportConfig.isAuthenticated, promotionController.getEditPromotion);
app.post("/promotion/edit/:id", passportConfig.isAuthenticated, promotionController.postEditPromotion);
app.post("/promotion/delete/:id", passportConfig.isAuthenticated, promotionController.postDeletePromotion);
app.get("/promotion/detail/:id", passportConfig.isAuthenticated, promotionController.getDetailPromotion);

/**
 * API examples routes.
 */
app.get("/api", apiController.getApi);
app.get("/api/facebook", passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getFacebook);

// Get keyword
app.get("/api/autoanswer", apiController.getAutoAnswer);

/**
 * OAuth authentication routes. (Sign in)
 */
app.get("/auth/facebook", passport.authenticate("facebook", { scope: ["email", "public_profile"] }));
app.get("/auth/facebook/callback", passport.authenticate("facebook", { failureRedirect: "/login" }), (req, res) => {
  res.redirect(req.session.returnTo || "/");
});

export default app;