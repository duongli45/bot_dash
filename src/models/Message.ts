import bcrypt from "bcrypt-nodejs";
import crypto from "crypto";
import mongoose from "mongoose";

export type MessageModel = mongoose.Document & {
  message_id: string,
  message_language: string,
  message_content: string
};

const messageSchema = new mongoose.Schema({
  message_id: String,
  message_language: String,
  message_content: String
}, { timestamps: true });

// export const User: UserType = mongoose.model<UserType>('User', userSchema);
const Message = mongoose.model("Message", messageSchema);
export default Message;
