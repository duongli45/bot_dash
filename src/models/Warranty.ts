import bcrypt from "bcrypt-nodejs";
import crypto from "crypto";
import mongoose from "mongoose";

export type WarrantyModel = mongoose.Document & {
  user_id: string,
  request_date: string,
  messages: [
    {
      user: string,
      message_content: string,
      datetime: Date
    }
  ],
  status: string
};

const warrantySchema = new mongoose.Schema({
  user_id: String,
  request_date: String,
  messages: [
    {
      user: String,
      message_content: String,
      datetime: Date
    }
  ],
  status: String
}, { timestamps: true });

const Warranty = mongoose.model("Warranty", warrantySchema);
export default Warranty;
