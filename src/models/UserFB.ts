import bcrypt from "bcrypt-nodejs";
import crypto from "crypto";
import mongoose from "mongoose";

export type UserFBModel = mongoose.Document & {
  first_name: String,
  last_name: String,
  profile_pic: String,
  locale: String,
  timezone: Number,
  gender: String,
  id: String,
  service_type: Number, // 0: fb
  status: { type: Number, required: true, default: 1 },
  added: Date,
  from_date: Date
};

const userFBSchema = new mongoose.Schema({
  first_name: String,
  last_name: String,
  profile_pic: String,
  locale: String,
  timezone: Number,
  gender: String,
  id: String,
  service_type: Number, // 0: fb
  status: { type: Number, required: true, default: 1 },
  added: Date,
  from_date: Date
}, { timestamps: true });

// export const User: UserType = mongoose.model<UserType>('User', userSchema);
const UserFB = mongoose.model("UserFB", userFBSchema);
export default UserFB;
