import mongoose from "mongoose";

export type TemplateModel = mongoose.Document & {
  template_id: string,
  template_name: string,
  image_url: string,
  additional_text: string,
  detail_url: string,
  type: string,
  parent_id: string,
  start_date: Date,
  end_date: Date,
  message: string
};

const templateSchema = new mongoose.Schema({
  template_id: String,
  template_name: String,
  image_url: String,
  additional_text: String,
  detail_url: String,
  type: String,
  parent_id: String,
  start_date: Date,
  end_date: Date,
  message: String
}, { timestamps: true });

const Template = mongoose.model("Template", templateSchema);
export default Template;
