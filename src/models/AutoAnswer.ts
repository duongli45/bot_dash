import mongoose from "mongoose";

export type AutoAnswerModel = mongoose.Document & {
  keyword: string,
  message_language: string,
  message_content: string
};

const autoAnswerSchema = new mongoose.Schema({
  keyword: String,
  message_language: String,
  message_content: String
}, { timestamps: true });

const AutoAnswer = mongoose.model("AutoAnswer", autoAnswerSchema);
export default AutoAnswer;
